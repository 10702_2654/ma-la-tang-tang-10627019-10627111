package sample;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import javax.swing.*;
import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public ComboBox kind;
    public ComboBox like;
    public ImageView picture;
    public GridPane main;
    public Label tt;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<String> strings = new ArrayList<>();
        strings.add("哺乳類");
        strings.add("鳥類");
        strings.add("魚類");
        kind.setItems(FXCollections.observableArrayList(strings));
        List<String> stringss = new ArrayList<>();
        stringss.add("草食性");
        stringss.add("肉食性");
        like.setItems(FXCollections.observableArrayList(stringss));
        //System.out.println(kind.getItems());

    }
    public void selkind(ActionEvent actionEvent) {
        InputStream inputStream = getClass().getResourceAsStream("/img/tenor.gif");
        picture.setImage(new Image(inputStream));
        tt.setText("                         ");
    }
    public void sellike(ActionEvent actionEvent) {
        if(kind.getSelectionModel().getSelectedItem()=="哺乳類" && like.getSelectionModel().getSelectedItem()=="肉食性") {
            InputStream inputStream = getClass().getResourceAsStream("/img/台灣黑熊.jpg");
            picture.setImage(new Image(inputStream));
            tt.setText("台灣黑熊");

            }
        if(kind.getSelectionModel().getSelectedItem()=="哺乳類" && like.getSelectionModel().getSelectedItem()=="草食性") {
            InputStream inputStreamm = getClass().getResourceAsStream("/img/無尾熊.jpg");
            picture.setImage(new Image(inputStreamm));
            tt.setText("無尾熊");
        }
        if(kind.getSelectionModel().getSelectedItem()=="鳥類" && like.getSelectionModel().getSelectedItem()=="肉食性") {
            InputStream inputStreamm = getClass().getResourceAsStream("/img/禿鷹.jpg");
            picture.setImage(new Image(inputStreamm));
            tt.setText("禿鷹");
        }
        if(kind.getSelectionModel().getSelectedItem()=="鳥類" && like.getSelectionModel().getSelectedItem()=="草食性") {
            InputStream inputStreamm = getClass().getResourceAsStream("/img/台灣藍鵲.jpg");
            picture.setImage(new Image(inputStreamm));
            tt.setText("台灣藍鵲");
        }
        if(kind.getSelectionModel().getSelectedItem()=="魚類" && like.getSelectionModel().getSelectedItem()=="肉食性") {
            InputStream inputStreamm = getClass().getResourceAsStream("/img/食人魚.jpeg");
            picture.setImage(new Image(inputStreamm));
            tt.setText("食人魚");
        }
        if(kind.getSelectionModel().getSelectedItem()=="魚類" && like.getSelectionModel().getSelectedItem()=="草食性") {
            InputStream inputStreamm = getClass().getResourceAsStream("/img/武昌魚.jfif");
            picture.setImage(new Image(inputStreamm));
            tt.setText("武昌魚");
        }
    }


}
